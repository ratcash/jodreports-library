[![pipeline status](https://gitlab.com/ratcash/jodreports-library/badges/master/pipeline.svg)](https://gitlab.com/ratcash/jodreports-library/commits/master) [![coverage report](https://gitlab.com/ratcash/jodreports-library/badges/master/coverage.svg)](https://gitlab.com/ratcash/jodreports-library/commits/master)

This is JODReports version 3.0.1, released on 2018-11-14

Old (but probably only) documentation: http://sourceforge.net/projects/jodreports/
  

Requirements
============

JODReports requires Java 1.4 or later (although with minor modifications
it can be made to work with 1.3 as well).

To create documents in formats other than OpenDocument Text (odt) 
please use JODConverter library
See http://www.artofsolving.com/opensource/jodconverter for more info, or
it's more recent, and maintained fork: 
https://github.com/sbraconnier/jodconverter

To run the webapp a servlet 2.3 container such as Apache Tomcat 4.1
or later is also required.

Licenses
========

The JODReports library is distributed under the terms of the LGPL.
This basically means that you are free to use it in both open source
and commercial projects.

If you modify the library itself you are required to contribute
your changes back, so JODReports can be improved.

(You are free to modify the sample webapp as a starting point for your
own webapp without restrictions.)

JODReports includes the following third-party libraries so you must
agree to their respective licenses as well

 * FreeMarker - http://www.freemarker.org
   BSD-style

 * Jakarta Commons - IO and FileUpload - http://jakarta.apache.org/commons/
   Apache License 2.0

 * XOM - http://xom.nu
   GNU LGPL

 * slf4j - http://slf4j.org
   MIT License

In compliance to some of the above licenses I also need to state here
that JODReports includes software developed by

 * the Visigoth Software Society (FreeMarker - http://www.visigoths.org/)
 * the Apache Software Foundation (http://www.apache.org)
 * the Spring Framework project (http://www.springframework.org)

-- Mirko Nasato (mirko at artofsolving.com)
-- Terry Liang (terry at poloniouslive.com)
